  /*
  *Ejercicio 1 Curso de introducción a la programacion  
  *Programa para mostrar los números del 1 al 100
  *que son pares
  *
  *Erika Lizbeth Hernández Morales  
  */
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>

	 
     int main(){
        for (int i = 0; i <= 100; i++){
			if (i %2 == 0)                        //operacion modulo para saber si es o no par.
				printf("%i\n", i) ;
		}
        
		return 0;
	 }

