 /*
  *Ejercicio 6 Curso de introducción a la programacion.  
  *Programa que muestra el perimetro de un triangulo isoceles.
  *El programa requiere como argumento dos números, el 
  *primer argumento debe ser el valor del lado repetido.
  *
  *Erika Lizbeth Hernández Morales  
  */
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>

int main(int argc , int** argv){
	if (argc < 3 ){
		 printf("Debes decirme los valores de cada lado; el primer valor debe ser el del lado repetido.");
		 exit(1);
	} else{       //el usuario ingreso el valor de todos los lados
		    int lado1;              //valor del lado 1 del triangulo
			int lado2;              //valor del lado 2 del triangulo
			int perimetro;
			sscanf(argv[1],"%i",&lado1); 
			sscanf(argv[2],"%i",&lado2); 
			
			if(lado1 <= 0 || lado2 <= 0){                 //Verificar que los lados sean positivos
			   printf("Un trinagulo no puede tener lados negativos");
			   exit(1);
			}else {
			   printf("El lado1 de tu triangulo vale:%i\n", lado1);
			   printf("El lado2 de tu triangulo vale:%i\n", lado1);
			   printf("El lado3 de tu triangulo vale:%i\n", lado2);
			   perimetro = (lado1*2) + lado2 ;
			   printf("El perimetro del triangulo es:%i\n", perimetro);
			}
	}   
	return 0;
}