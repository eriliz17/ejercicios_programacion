 /*
  *Ejercicio 5 Curso de introducción a la programacion  
  *Programa que muestra el perimetro de un triangulo escaleno
  *El programa requiere como argumento tres números
  *
  *Erika Lizbeth Hernández Morales  
  */
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>

int main(int argc , char** argv){
	if (argc < 4 ){
		 printf("Necesitas darme el valor de cada lado de tu triangulo escaleno");
		 exit(1);
	} else{       //el usuario ingreso el valor de todos los lados
		    int lado1;              //valor del lado 1 del triangulo
			int lado2;              //valor del lado 2 del triangulo
			int lado3;              //valor del lado 3 del triangulo
			int perimetro;
			sscanf(argv[1],"%i",&lado1); 
			sscanf(argv[2],"%i",&lado2); 
			sscanf(argv[3],"%i",&lado3); 
			
			if((lado1>0 && lado2>0) && lado3>0){
				printf("El lado1 de tu triangulo vale:%i\n", lado1);
			    printf("El lado2 de tu triangulo vale:%i\n", lado2);
			    printf("El lado3 de tu triangulo vale:%i\n", lado3);
			    perimetro = lado1 + lado2 +lado3;
			    printf("El perimetro del triangulo es:%i\n", perimetro);
				
			}else{
			    printf("Los triangulos no tienen lados negativos, vuelve a intentar");
				exit(1);
			}
	}   
	return 0;
}
		   