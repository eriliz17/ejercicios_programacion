  /*
  *Ejercicio 2 Curso de introducción a la programacion  
  *Programa que suma los numeros consecutivos desde
  *un numero indicado por el usuario hasta 100.
  *El programa requiere como argumento un número entre
  * el 0 y el 100
  *
  *Erika Lizbeth Hernández Morales  
  */
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>

int main(int argc , char** argv){
	if (argc < 2 ){
		 printf("Especifica desde que número debo sumar");
		 exit(1);
	} else{       //el usuario ingreso un numero
		    int base;              //numero ingresado por el usuario
			int suma = 0;        //Variable acomulable
			sscanf(argv[1],"%i",&base); 
             if (argc < 0 && argc > 50)  { //El numero no esta entre 0 y 50
	              printf("Tu numero debe estar entre 0 y 50, vuelve a intentar");
				  exit(1);
			 }else{
		         for(int i = 0; i <= base; i++){
			        suma = suma + i;       //Se efectua la suma 
		        }
		   }
		   printf("La suma es:%i\n" , suma);   //Muestra el resultado de la suma 
	}
	
	return 0;
}