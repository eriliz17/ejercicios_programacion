 /*
  *Ejercicio 4 Curso de introducción a la programacion  
  *Programa que muestra el area de un triangulo equilatero
  *El programa requiere como argumento un número 
  *
  *Erika Lizbeth Hernández Morales  
  */
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>

int main(int argc , char** argv){
	if (argc < 2 ){
		 printf("Necesitas darme el valor de un lado de tu triangulo equilatero");
		 exit(1);
	} else{       //el usuario ingreso un numero
		    int valor;              //valor del los lados del triangulo
			float perimetro;
			sscanf(argv[1],"%i",&valor); 
			
			if(valor < 0){
			   printf("El valor de cada lado del triangulo no puede ser negativo");
			   exit(1);
			}else{
			   printf("Los lados de tu trianguloequilatero valen:%i\n", valor);
			   perimetro = valor * 3;
			   printf("El perimetro del triangulo es:%f\n", perimetro);
			}
	}   
	return 0;
}
		   