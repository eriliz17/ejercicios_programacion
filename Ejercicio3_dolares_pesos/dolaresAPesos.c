 /*
  *Ejercicio 3 Curso de introducción a la programacion  
  *Programa que indica la cantidad en pesos MXN de los
  *dolares que introduzcas
  *El programa requiere como argumento un número 
  *
  *Erika Lizbeth Hernández Morales  
  */
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>

int main(int argc , char** argv){
	if (argc < 2 ){
		 printf("Dime cuantos dolares tienes");
		 exit(1);
	}
	else{       //el usuario ingreso un numero
		    int dolares;              //numero de dolares del usuario
			sscanf(argv[1],"%i",&dolares); 
			if (dolares < 0){
				 printf("El monto de dolares deebe ser positivo");
		         exit(1);
			}else{
			    float pesos = dolares * 22.36;  //Tipo de cambio del dolar al dia 11/08/20
			    printf("La cantidad de pesos que tienes es:%f\n", pesos);
	}   
	return 0;
}
		   